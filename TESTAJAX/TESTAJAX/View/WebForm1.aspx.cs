﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TESTAJAX
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [System.Web.Services.WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static List<Person> test()
        {
            List<Person> lstPs = new List<Person>();

            lstPs.Add(new Person { Empid = 1, EmpName = "Emp1", EmpLastName = "EmpLast1" });
            lstPs.Add(new Person { Empid = 2, EmpName = "Emp2", EmpLastName = "EmpLast2" });
            lstPs.Add(new Person { Empid = 3, EmpName = "Emp3", EmpLastName = "EmpLast3" });


            return lstPs.ToList();
        }
        //[System.Web.Services.WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public static List<Person> Update(List<Person> lstPst)
        //{
        //    return lstPst.ToList();
        //}
        //public class Person
        //{
        //    public int Empid { get; set; }
        //    public string EmpName { get; set; }
        //    public string EmpLastName { get; set; }
        //}
    }
}