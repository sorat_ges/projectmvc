﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TESTAJAX.Startup))]
namespace TESTAJAX
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
