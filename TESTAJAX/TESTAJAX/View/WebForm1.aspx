﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="TESTAJAX.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title></title>
    <link href="../Content/bootstrap.css" rel="stylesheet" />
    <link href="../Content/StyleSheetDataTable.css" rel="stylesheet" />
   <link href="../Content/StyleSheetHeader.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="True">
        </asp:ScriptManager>

        <div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="bs-carousel">
            <!-- Overlay -->
            <div class="overlay"></div>

            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#bs-carousel" data-slide-to="0" class="active"></li>
                <li data-target="#bs-carousel" data-slide-to="1"></li>
                <li data-target="#bs-carousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item slides active">
                    <div class="slide-1"></div>
                    <div class="hero">
                        <hgroup>
                            <h1>We are creative</h1>
                            <h3>Get start your next awesome project</h3>
                        </hgroup>
                        <button class="btn btn-hero btn-lg" role="button">See all features</button>
                    </div>
                </div>
                <div class="item slides">
                    <div class="slide-2"></div>
                    <div class="hero">
                        <hgroup>
                            <h1>We are smart</h1>
                            <h3>Get start your next awesome project</h3>
                        </hgroup>
                        <button class="btn btn-hero btn-lg" role="button">See all features</button>
                    </div>
                </div>
                <div class="item slides">
                    <div class="slide-3"></div>
                    <div class="hero">
                        <hgroup>
                            <h1>We are amazing</h1>
                            <h3>Get start your next awesome project</h3>
                        </hgroup>
                        <button class="btn btn-hero btn-lg" role="button">See all features</button>
                    </div>
                </div>
            </div>
        </div>
      
        <div>
        </div>
      <div class="col-lg-10 col-lg-offset-2">

        <div class="row col-lg-12">
            <div class="col-lg-10 col-centered">
                <table id="table1" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>EmpID</th>
                            <th>EmpName</th>
                            <th>EmpLastName</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Data</h4>
                    </div>
                    <div>
                        <div class="modal-body">
                            <label for="txtEmpID">EmpID</label>
                            <input class="form-control" id="txtEmpID" type="text" />
                            <label for="txtEmpName">EmpName</label>
                            <input class="form-control" id="txtEmpName" type="text" />
                            <label for="txtEmpLast">EmpLastName</label>
                            <input class="form-control" id="txtEmpLast" type="text" />
                        </div>
                        <div id="showalert" class="alert alert-success" role="alert">
                            <strong>Well done!</strong> You successfully read this important alert message.
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-success">Save</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
          
      </div>
             
    </form>
</body>


<script type="text/javascript" src="../Scripts/jquery-3.2.1.js"></script>
<script src="../Scripts/jquerry.dataTables.min.js"></script>
<script src="../Scripts/bootstrap.js"></script>
<script src="../Scripts/bootstrap.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

        $.ajax({
            type: 'POST',
            url: "WebForm1.aspx/test",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (data) {
                $('#table1').append("<tbody></tbody>");
                $.each(data.d, function (i, value) {
                    var body = "<tr><td>" + value.Empid + "</td><td>" + value.EmpName + "</td><td>" + value.EmpLastName + "</td> ";
                    body += "<td><button type='button' id='BtnModal' class='btn btn-info btn-lg' data-toggle='modal' >Edit</button></td></tr>"
                    $('#table1').append(body);
                });
                //$("#table1").DataTable();
                //

            }, error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            }, complete: function (data) {
                $('#table1').DataTable({
                    "pagingType": "first_last_numbers"
                 , "pageLength": 10, bInfo: false, "bSort": false,
                    "columnDefs": [{ "className": "dt-center", "targets": "_all" }],
                });

            }



        });
        $(document).on("click", "tbody #BtnModal", function () {
            //alert($(this).closest('tbody tr').children('td:eq(0)').text() + ',' + $(this).closest('tbody tr').children('td:eq(1)').text())
            $('#txtEmpID').val($(this).closest('tbody tr').children('td:eq(0)').text());
            $('#txtEmpName').val($(this).closest('tbody tr').children('td:eq(1)').text());
            $('#txtEmpLast').val($(this).closest('tbody tr').children('td:eq(2)').text());
            $('.modal-body').show();
            $('#showalert').hide();
            $('#myModal').modal({});
        });

        $('.btn-success').click(function () {
            var lst = [{
                Empid: $('#txtEmpID').val(),
                EmpName: $('#txtEmpName').val(),
                EmpLastName: $('#txtEmpLast').val()
            }];
            lst = JSON.stringify({ 'lstPst': lst });
            $.ajax({
                type: 'POST',
                url: "View/WebForm1.aspx/Update",
                contentType: 'application/json; charset=utf-8',
                data: lst,
                dataType: 'json',
                success: function (data) {

                    $('#table1 tbody tr').each(function () {
                        var customerId = $(this).find("td").html();
                        alert(customerId);
                    });
                    //$("#table1").DataTable();
                    //

                }, error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
                }, complete: function (data) {

                }


                //$("#EditValue").click(function () {
                //    alert('a');
                //    var myClass = $(this).attr("class");
                //    alert(myClass);
                //});
            });
        });
    });

   
</script>

</html>
